=======
sb_slider
=======

sb_slider is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds slider to page.


Installation
============

sb_slider requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_slider in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_slider


Configuration 
=============

1. Add "sb_link" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_slider',
        ...
    )

2. Run `python manage.py migrate` to create the sb_slider models.
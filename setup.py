import os
from setuptools import setup, find_packages
from sb_slider import __version__

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='sb_slider',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    license='MIT License',
    platforms=['OS Independent'],
    description='This django-cms plugin adds image slider to page',
    long_description=README,
    url='https://vuspace.pro/',
    download_url='https://bitbucket.org/vivazzi/sb_slider/downloads/',
    author='Artem Maltsev',
    author_email='maltsev.artjom@gmail.com',
    keywords='django django-cms sbl image slider',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', 
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'django-admin-sortable2',
    ],
)

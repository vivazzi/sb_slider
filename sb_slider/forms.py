from cms.forms.fields import PageSmartLinkField
from django.utils.translation import get_language
from cms.utils.i18n import get_language_tuple
from django import forms
from django.forms.widgets import HiddenInput

from sb_core import settings
from sb_slider.models import SBSlider, SBSliderPicture


class SBSliderForm(forms.ModelForm):
    class Meta:
        model = SBSlider
        exclude = ()

    class Media:
        js = [settings.JQUERY_URL, 'sb_core/js/sb_library.js', 'sb_slider/sb_slider_admin.js', ]


class SBSliderPictureForm(forms.ModelForm):
    link = PageSmartLinkField(label='Ссылка на страницу', required=False, placeholder_text='Начните набирать...',
                              ajax_view='admin:cms_page_get_published_pagelist')

    language = forms.ChoiceField(label='Язык', choices=get_language_tuple())

    class Meta:
        model = SBSliderPicture
        exclude = ('language',)

        widgets = {
            'description': forms.Textarea(attrs={'cols': 50, 'rows': 3}),
        }

    def __init__(self, *args, **kwargs):
        super(SBSliderPictureForm, self).__init__(*args, **kwargs)
        self.fields['language'].widget = HiddenInput()

        if not self.fields['language'].initial:
            self.fields['language'].initial = get_language()

        if 'link' in self.fields:
            self.fields['link'].widget.language = self.fields['language'].initial

from adminsortable2.admin import SortableInlineAdminMixin
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from django.contrib import admin
from sb_core.constants import BASE

from sb_slider.forms import SBSliderPictureForm, SBSliderForm
from sb_slider.models import SBSlider, SBSliderPicture


class SBSliderPictureInline(SortableInlineAdminMixin, admin.TabularInline):
    model = SBSliderPicture
    form = SBSliderPictureForm
    extra = 0


class SBSliderPlugin(CMSPluginBase):
    module = BASE
    model = SBSlider
    name = 'Слайдер'
    form = SBSliderForm
    render_template = 'sb_slider/sb_slider.html'

    inlines = (SBSliderPictureInline, )

    fieldsets = (
        ('', {
            'fields': ('title', ('height', 'width'), 'show_paginator', 'hide_navs', 'show_hint')
        }),
        ('Настройка смены слайдов', {
            'fields': (('is_move_slide', 'stopper', 'move_speed', 'auto_speed'),
                       'loop')
        }),
        ('Внешний вид слайдера', {
            'fields': (('is_shadow', 'shadow_pars'),
                       ('is_border', 'border_size', 'border_color'),
                       'bg_color')
        })
    )

    def render(self, context, instance, placeholder):
        context = super(SBSliderPlugin, self).render(context, instance, placeholder)
        context['active_pics'] = instance.pics.active()

        return context

plugin_pool.register_plugin(SBSliderPlugin)

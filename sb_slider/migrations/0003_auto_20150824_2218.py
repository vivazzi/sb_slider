from django.db import models, migrations
import sb_core.folder_mechanism


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0002_auto_20150818_2107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='pic',
            field=models.ImageField(help_text='\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u0442\u044c \u0444\u043e\u0442\u043e \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 300 KB', upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=250, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
    ]

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0010_set_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbslider',
            name='show_paginator',
            field=models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u043d\u0443\u043c\u0435\u0440\u0430\u0446\u0438\u044e \u0441\u043b\u0430\u0439\u0434\u043e\u0432?'),
        ),
    ]

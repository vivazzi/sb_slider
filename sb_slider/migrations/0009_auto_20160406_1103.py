from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0008_auto_20160406_1058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbslider',
            name='bg_color',
            field=models.CharField(help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', max_length=100, null=True, verbose_name='\u0426\u0432\u0435\u0442 \u0444\u043e\u043d\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='is_shadow',
            field=models.BooleanField(default=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0442\u0435\u043d\u044c?'),
        ),
    ]

from django.db import models, migrations


def generate_order(apps, schema_editor):
    SBSlider = apps.get_model('sb_slider', 'SBSlider')
    for slider in SBSlider.objects.all():
        for i, obj in enumerate(slider.pics.all(), start=1):
            obj.order = i
            obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0009_auto_20160406_1103'),
    ]

    operations = [
        migrations.RunPython(generate_order),
    ]

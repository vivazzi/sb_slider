from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0007_auto_20160324_1619'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sbsliderpicture',
            options={'ordering': ('order',), 'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0435', 'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0435'},
        ),
        migrations.RenameField(
            model_name='sbslider',
            old_name='background_color',
            new_name='bg_color'
        ),
        migrations.RenameField(
            model_name='sbslider',
            old_name='shadow',
            new_name='is_shadow'
        ),
        migrations.AddField(
            model_name='sbslider',
            name='is_border',
            field=models.BooleanField(default=True, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0433\u0440\u0430\u043d\u0438\u0446\u0443?'),
        ),
        migrations.AddField(
            model_name='sbslider',
            name='is_move_slide',
            field=models.BooleanField(default=True, verbose_name='\u0410\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u0441\u043c\u0435\u043d\u0430 \u0441\u043b\u0430\u0439\u0434\u043e\u0432?'),
        ),
        migrations.AddField(
            model_name='sbslider',
            name='shadow_pars',
            field=models.CharField(default='0 0 10px 0 rgba(0,0,0,0.5)', max_length=100, blank=True, help_text='<\u0441\u0434\u0432\u0438\u0433 \u043f\u043e x> <\u0441\u0434\u0432\u0438\u0433 \u043f\u043e y> <\u0440\u0430\u0434\u0438\u0443\u0441 \u0440\u0430\u0437\u043c\u044b\u0442\u0438\u044f> <\u0440\u0430\u0441\u0442\u044f\u0436\u0435\u043d\u0438\u0435> <\u0446\u0432\u0435\u0442></br>\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: 0 0 10px 0 rgba(0, 0, 0, 0.5), 2px 2px 7px 1px rgba(0, 0, 0, 0.4)', null=True, verbose_name='\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u0442\u0435\u043d\u0438'),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='auto_speed',
            field=models.PositiveIntegerField(default=4000, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0441\u043b\u0430\u0439\u0434\u0430, \u043c\u0441', blank=True, validators=[django.core.validators.MinValueValidator(1000)]),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='border_color',
            field=models.CharField(default='#fff', max_length=255, blank=True, help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', null=True, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446'),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='border_size',
            field=models.PositiveSmallIntegerField(default=3, null=True, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u0433\u0440\u0430\u043d\u0438\u0446, \u043f\u0438\u043a\u0441', blank=True, validators=[django.core.validators.MinValueValidator(1)]),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='move_speed',
            field=models.PositiveIntegerField(default=500, null=True, verbose_name='\u0421\u043a\u043e\u0440\u043e\u0441\u0442\u044c \u0441\u043c\u0435\u043d\u044b \u0441\u043b\u0430\u0439\u0434\u0430, \u043c\u0441', blank=True, validators=[django.core.validators.MinValueValidator(100)]),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='stopper',
            field=models.BooleanField(default=True, help_text='\u0414\u043b\u044f \u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0438/\u0437\u0430\u043f\u0443\u0441\u043a\u0430 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u0441\u043c\u0435\u043d\u044b \u0441\u043b\u0430\u0439\u0434\u043e\u0432', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043a\u043d\u043e\u043f\u043a\u0443 "\u043f\u0430\u0443\u0437\u0430/\u043f\u043b\u0435\u0439"'),
        ),
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='order',
            field=models.PositiveIntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a', db_index=True),
        ),
    ]

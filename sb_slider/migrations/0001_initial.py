from django.db import models, migrations

from sb_core import folder_mechanism


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20140926_2347'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBSlider',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('title', models.CharField(help_text='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u0438\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('height', models.PositiveIntegerField(default=200, verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430, \u043f\u0438\u043a\u0441\u0435\u043b\u0438')),
                ('loop', models.BooleanField(default=False, help_text='\u0421\u0442\u0440\u0435\u043b\u043a\u0438 \u0432\u043b\u0435\u0432\u043e/\u0432\u043f\u0440\u0430\u0432\u043e \u043f\u0440\u043e\u043f\u0430\u0434\u0430\u0442\u044c \u043d\u0435 \u0431\u0443\u0434\u0443\u0442', verbose_name='\u0417\u0430\u0446\u0438\u043a\u043b\u0438\u0432\u0430\u043d\u0438\u0435')),
                ('auto_speed', models.PositiveIntegerField(default=4000, help_text='\u0415\u0441\u043b\u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0441\u043b\u0430\u0439\u0434\u0430 \u043d\u0435 \u0437\u0430\u0434\u0430\u043d\u043e (\u0438\u043b\u0438 \u0440\u0430\u0432\u043d\u043e 0), \u0442\u043e \u0441\u043c\u0435\u043d\u0430 \u0441\u043b\u0430\u0439\u0434\u043e\u0432 \u043d\u0435 \u0431\u0443\u0434\u0435\u0442 \u043f\u0440\u043e\u0438\u0441\u0445\u043e\u0434\u0438\u0442\u044c. <br/> \u0412\u0440\u0435\u043c\u044f \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0432 \u043c\u0438\u043b\u0438\u0441\u0435\u043a\u0443\u043d\u0434\u0430\u0445', null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0441\u043b\u0430\u0439\u0434\u0430, \u043c\u0441\u0435\u043a', blank=True)),
                ('stopper', models.BooleanField(default=True, help_text='\u041f\u0440\u0438 \u0432\u044b\u0431\u043e\u0440\u0435 \u0434\u0430\u043d\u043d\u043e\u0433\u043e \u043f\u043e\u043b\u044f \u043c\u043e\u0436\u043d\u043e \u0431\u0443\u0434\u0435\u0442 \u0430\u0432\u0442\u043e\u043f\u0440\u043e\u043a\u0440\u0443\u0442\u043a\u0443 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430 \u043e\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u044c \u043a\u043d\u043e\u043f\u043a\u043e\u0439 "\u043f\u0430\u0443\u0437\u0430/\u043f\u043b\u0435\u0439". <br/> \u0414\u0430\u043d\u043d\u0430\u044f \u043e\u043f\u0446\u0438\u044f \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442, \u0435\u0441\u043b\u0438 \u0437\u0430\u0434\u0430\u043d\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 "\u0412\u0440\u0435\u043c\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0441\u043b\u0430\u0439\u0434\u0430', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043a\u043d\u043e\u043f\u043a\u0443 "\u043f\u0430\u0443\u0437\u0430/\u043f\u043b\u0435\u0439"')),
                ('move_speed', models.PositiveIntegerField(default=500, help_text='\u0412\u0440\u0435\u043c\u044f \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0432 \u043c\u0438\u043b\u0438\u0441\u0435\u043a\u0443\u043d\u0434\u0430\u0445', verbose_name='\u0421\u043a\u043e\u0440\u043e\u0441\u0442\u044c \u0441\u043c\u0435\u043d\u044b \u0441\u043b\u0430\u0439\u0434\u0430, \u043c\u0441\u0435\u043a')),
                ('shadow', models.BooleanField(default=True, verbose_name='\u0422\u0435\u043d\u044c \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('border_size', models.PositiveSmallIntegerField(default=3, help_text='\u0415\u0441\u043b\u0438 \u0443\u043a\u0430\u0437\u0430\u043d\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 0, \u0442\u043e \u0433\u0440\u0430\u043d\u0438\u0446\u044b \u043d\u0435 \u0431\u0443\u0434\u0435\u0442.', verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u0433\u0440\u0430\u043d\u0438\u0446 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('border_color', models.CharField(default=b'#fff', help_text='\u0426\u0432\u0435\u0442 \u043c\u043e\u0436\u0435\u0442 \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u0442\u044c\u0441\u044f \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u044b\u043c\u0438 \u0432\u0430\u0440\u0438\u0430\u043d\u0442\u0430\u043c\u0438, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u043c\u043e\u0433\u0443\u0442 \u043f\u0440\u043e\u0447\u0438\u0442\u0430\u0442\u044c \u0431\u0440\u0430\u0443\u0437\u0435\u0440\u044b. <br/>\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, #F00, rgb(255,0,0), rgb(100%,0%, 0%), rgba(255, 0, 0, 0.5), hsl(0, 100%, 50%), hsla(120, 100%, 50%, 0.5) \u0438 \u0442.\u0434.', max_length=255, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446')),
                ('background_color', models.CharField(default=b'#fff', help_text='\u0412\u0438\u0434\u0435\u043d, \u0435\u0441\u043b\u0438 \u0432 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f\u0445 \u0435\u0441\u0442\u044c \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430 <br/>\u0426\u0432\u0435\u0442 \u043c\u043e\u0436\u0435\u0442 \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u0442\u044c\u0441\u044f \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u044b\u043c\u0438 \u0432\u0430\u0440\u0438\u0430\u043d\u0442\u0430\u043c\u0438, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u043c\u043e\u0433\u0443\u0442 \u043f\u0440\u043e\u0447\u0438\u0442\u0430\u0442\u044c \u0431\u0440\u0430\u0443\u0437\u0435\u0440\u044b. <br/>\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, #F00, rgb(255,0,0), rgb(100%,0%, 0%), rgba(255, 0, 0, 0.5), hsl(0, 100%, 50%), hsla(120, 100%, 50%, 0.5) \u0438 \u0442.\u0434.', max_length=100, verbose_name='\u0446\u0432\u0435\u0442 \u0444\u043e\u043d\u0430 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
            ],
            options={
                'db_table': 'sb_slider',
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440',
                'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440\u044b',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SBSliderPicture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pic', models.ImageField(help_text='\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u0442\u044c \u0444\u043e\u0442\u043e \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 500 KB', upload_to=folder_mechanism.upload_to_handler, max_length=250, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('description', models.TextField(help_text='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0432\u043d\u0438\u0437\u0443 \u0441\u043b\u0430\u0439\u0434\u0430', null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('order', models.PositiveSmallIntegerField(null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a', blank=True)),
                ('folder', models.CharField(max_length=5, null=True, editable=False, blank=True)),
                ('page_link', models.ForeignKey(verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0441\u0430\u0439\u0442\u0430', blank=True, to='cms.Page', null=True, on_delete=models.CASCADE)),
                ('slider', models.ForeignKey(related_name='pictures', verbose_name='\u0421\u043b\u0430\u0439\u0434\u0435\u0440', to='sb_slider.SBSlider', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'sb_slider_picture',
                'verbose_name': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0435',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0435',
            },
            bases=(models.Model,),
        ),
    ]

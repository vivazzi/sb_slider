from django.db import models, migrations
import sb_core.fields


def set_blank(apps, schema_editor):
    SBSlider = apps.get_model('sb_slider', 'SBSlider')
    for obj in SBSlider.objects.all():
        obj.bg_color = obj.bg_color or ''
        obj.border_color = obj.border_color or ''
        obj.title = obj.title or ''
        obj.save()

    SBSliderPicture = apps.get_model('sb_slider', 'SBSliderPicture')
    for obj in SBSliderPicture.objects.all():
        obj.description = obj.description or ''
        obj.folder = obj.folder or ''
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0016_sbslider_width'),
    ]

    operations = [
        migrations.RunPython(set_blank),
        migrations.AlterField(
            model_name='sbslider',
            name='bg_color',
            field=sb_core.fields.ColorField(default='', help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430<br/><a rel="nofollow" target="_blank" href="http://getcolor.ru/">\u0421\u0435\u0440\u0432\u0438\u0441 \u043f\u043e \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044e \u0446\u0432\u0435\u0442\u0430</a>', max_length=30, verbose_name='\u0426\u0432\u0435\u0442 \u0444\u043e\u043d\u0430', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='border_color',
            field=sb_core.fields.ColorField(default='#fff', help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430<br/><a rel="nofollow" target="_blank" href="http://getcolor.ru/">\u0421\u0435\u0440\u0432\u0438\u0441 \u043f\u043e \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044e \u0446\u0432\u0435\u0442\u0430</a>', max_length=30, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446', blank=True),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='title',
            field=models.CharField(default='', help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='description',
            field=models.TextField(default='', help_text='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0432\u043d\u0438\u0437\u0443 \u0441\u043b\u0430\u0439\u0434\u0430', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='folder',
            field=models.CharField(default='', max_length=5, editable=False, blank=True),
            preserve_default=False,
        ),
    ]

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='slider',
            field=models.ForeignKey(related_name='pics', verbose_name='\u0421\u043b\u0430\u0439\u0434\u0435\u0440', to='sb_slider.SBSlider', on_delete=models.CASCADE),
        ),
    ]

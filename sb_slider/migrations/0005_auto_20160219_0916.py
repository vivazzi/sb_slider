from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0004_auto_20150901_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbslider',
            name='background_color',
            field=models.CharField(default=b'#fff', help_text='\u0412\u0438\u0434\u0435\u043d, \u0435\u0441\u043b\u0438 \u0432 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f\u0445 \u0435\u0441\u0442\u044c \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430 <br/>\u0426\u0432\u0435\u0442 \u043c\u043e\u0436\u0435\u0442 \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u0442\u044c\u0441\u044f \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u044b\u043c\u0438 \u0432\u0430\u0440\u0438\u0430\u043d\u0442\u0430\u043c\u0438, \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440:#545454, rgb(99,187,120), rgba(99, 0, 0, 0.5), hsla(120, 100%, 50%, 0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430.', max_length=100, verbose_name='\u0446\u0432\u0435\u0442 \u0444\u043e\u043d\u0430 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='border_color',
            field=models.CharField(default=b'#fff', help_text='\u0426\u0432\u0435\u0442 \u043c\u043e\u0436\u0435\u0442 \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u0442\u044c\u0441\u044f \u0440\u0430\u0437\u043b\u0438\u0447\u043d\u044b\u043c\u0438 \u0432\u0430\u0440\u0438\u0430\u043d\u0442\u0430\u043c\u0438, \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440:#545454, rgb(99,187,120), rgba(99, 0, 0, 0.5), hsla(120, 100%, 50%, 0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430.', max_length=255, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446'),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='title',
            field=models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]

from django.db import models, migrations
import sb_core.folder_mechanism


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0006_auto_20160311_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbslider',
            name='background_color',
            field=models.CharField(default=b'#fff', help_text='\u0412\u0438\u0434\u0435\u043d, \u0435\u0441\u043b\u0438 \u0432 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f\u0445 \u0435\u0441\u0442\u044c \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u044b\u0435 \u043c\u0435\u0441\u0442\u0430 <br/>\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', max_length=100, verbose_name='\u0446\u0432\u0435\u0442 \u0444\u043e\u043d\u0430 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='sbslider',
            name='border_color',
            field=models.CharField(default=b'#fff', help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', max_length=255, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446'),
        ),
        migrations.AlterField(
            model_name='sbsliderpicture',
            name='pic',
            field=models.ImageField(help_text='\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u0433\u0440\u0443\u0436\u0430\u0442\u044c \u0444\u043e\u0442\u043e \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 200 KB', upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=250, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
    ]

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_slider', '0017_auto_20160809_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbslider',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_slider_sbslider', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]

from os.path import basename

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from cms.models import CMSPlugin, Page
from django.utils import timezone

from sb_core.constants import pic_ht, title_ht, color_ht, shadow_ht
from sb_core.fields import PicField, ColorField

from sb_core.folder_mechanism import upload_to_handler
from sb_core.models import PicModel, copy_relations_fk

width_ht = ('При указании ширины слайдер будет располагаться по центру <br/>и пропорционально изменять свою высоту в зависимости '
            'от ширины экрана.<br/>При отсутствии ширины слайдер будет иметь фиксированную высоту и занимать всю ширину блока.')


class SBSlider(CMSPlugin):
    title = models.CharField('Название', max_length=255, blank=True, help_text=title_ht)
    height = models.PositiveIntegerField('Высота, пиксели', default=200)
    width = models.PositiveIntegerField('Ширина, пиксели', null=True, blank=True, help_text=width_ht)

    show_paginator = models.BooleanField('Показать нумерацию слайдов?', default=True)
    hide_navs = models.BooleanField('Скрывать навигационные элементы?', default=True,
                                    help_text='Если поле отмечено, то навигационные элементы показываются '
                                              'только при наведении мышкой на слайдер')
    show_hint = models.BooleanField('Всегда показывать подсказку слайдов?', default=False,
                                    help_text='Оставьте поле не отмеченным, если хотите показывать подсказку к '
                                              'слайдам только при наведении мышкой на них')

    # slide speeds
    is_move_slide = models.BooleanField('Автоматическая смена слайдов?', default=True)
    move_speed = models.PositiveIntegerField('Скорость смены слайда, мс', default=500, null=True, blank=True,
                                             validators=[MinValueValidator(100), ])
    auto_speed = models.PositiveIntegerField('Время показа слайда, мс', default=4000, null=True, blank=True,
                                             validators=[MinValueValidator(1000), ])
    stopper = models.BooleanField('Показывать кнопку "пауза/плей"', default=True,
                                  help_text='Для остановки/запуска автоматической смены слайдов')
    loop = models.BooleanField('Зацикливание', default=False, help_text='Стрелки влево/вправо пропадать не будут')

    # styles
    is_shadow = models.BooleanField('Добавить тень?', default=True)
    shadow_pars = models.CharField('Параметры тени', max_length=100, blank=True, null=True,
                                   default='0 0 10px 0 rgba(0,0,0,0.5)', help_text=shadow_ht)

    is_border = models.BooleanField('Добавить границу?', default=True)
    border_size = models.PositiveSmallIntegerField('Размер границ, пикс', default=3, null=True, blank=True,
                                                   validators=[MinValueValidator(1), ])
    border_color = ColorField('Цвет границ', blank=True, default='#fff', help_text=color_ht)

    bg_color = ColorField('Цвет фона', blank=True, help_text=color_ht)

    def copy_relations(self, old_instance):
        copy_relations_fk(self, old_instance, 'slider')

    @property
    def style(self):
        border = 'border: {}px solid {};'.format(self.border_size, self.border_color) if self.is_border else ''
        shadow = 'box-shadow: {};'.format(self.shadow_pars) if self.is_shadow else ''
        bg_color = 'background-color: {};'.format(self.bg_color) if self.bg_color else ''
        max_width = 'max-width: {}px;'.format(self.width) if self.width else ''
        return ''.join([border, shadow, bg_color, max_width])

    def clean(self):
        if self.is_move_slide and not (self.move_speed and self.auto_speed):
            raise ValidationError('При использовании автоматической смены слайдов укажите значения в обоих полях: '
                                  '"Скорость смены слайда", "Время показа слайда"')

        if self.is_shadow and not self.shadow_pars:
            raise ValidationError('При использовании тени укажите значение в поле "Параметры тени"')

        if self.is_border and not (self.border_size and self.border_color):
            raise ValidationError('При использовании границы укажите значения в обоих полях: "Размер границ", "Цвет границ"')

    def __str__(self):
        return self.title or ''

    class Meta:
        db_table = 'sb_slider'
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдеры'


class ActiveQuerySet(QuerySet):
    def active(self):
        return self.filter(is_active=True).filter(Q(publication_date__lte=timezone.now(), publication_end_date__gte=timezone.now()) |
                                                  Q(publication_date__lte=timezone.now(), publication_end_date__isnull=True))


class SBSliderPicture(PicModel):
    slider = models.ForeignKey(SBSlider, verbose_name='Слайдер', related_name='pics', on_delete=models.CASCADE)
    pic = PicField('Изображение', upload_to=upload_to_handler, help_text=pic_ht)
    description = models.TextField('Описание', blank=True, help_text='Показывается внизу слайда')

    order = models.PositiveIntegerField('Порядок', db_index=True)

    link = models.CharField('Ссылка на страницу', max_length=255, blank=True)

    is_active = models.BooleanField('Активный?', default=True,
                                    help_text='Снимите галочку, если хотите на время не показывать содержимое '
                                              '(вне зависимости от дат показа).')

    publication_date = models.DateTimeField('Дата показа', null=True, blank=True,
                                            help_text='Если значение не задано, то будет использоваться текущее время.')
    publication_end_date = models.DateTimeField('Дата окончания показа', null=True, blank=True,
                                                help_text='Срок показа содержимого. Оставьте пустым для бесконечного срока.')

    objects = ActiveQuerySet.as_manager()

    def is_enabled(self):
        if self.is_active:
            now = timezone.now()
            if self.publication_end_date and self.publication_date <= now <= self.publication_end_date:
                return True
            elif not self.publication_end_date and self.publication_date <= now:
                return True

        return False

    def save(self, no_signals=False, *args, **kwargs):
        if not self.publication_date:
            self.publication_date= timezone.now()
        super(SBSliderPicture, self).save(*args, **kwargs)

    def __str__(self):
        return basename(self.pic.name)

    class Meta:
        ordering = ('order', )
        db_table = 'sb_slider_picture'
        verbose_name = 'Изображение в слайдере'
        verbose_name_plural = 'Изображения в слайдере'

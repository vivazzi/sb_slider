$(function(){
    var manager_el = $('#id_is_move_slide');
    var active_toggle_elements = $('#id_stopper, #id_move_speed, #id_auto_speed');
    active_toggle(manager_el, active_toggle_elements, true);

    manager_el = $('#id_is_border');
    active_toggle_elements = $('#id_border_size, #id_border_color');
    active_toggle(manager_el, active_toggle_elements, true);

    active_toggle($('#id_is_shadow'), $('#id_shadow_pars'), true);
});
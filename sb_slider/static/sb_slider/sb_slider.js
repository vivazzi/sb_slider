/* SBL: Slider (sb_slider.js)
 * Copyright (c) 2013 - 2016 Maltsev Artem, maltsev.artjom@gmail.com, vivazzi.pro
 * web studio: Viva IT Studio, vits.pro
 */
(function($){
    $.fn.sb_slider = function(method) {
        var opt = {
            'index': 0,
            'is_move_slide': true,
            'auto_speed': 4000,
            'move_speed': 500,
            'loop': false,
            'show_stopper': true
        };

        function isNumber(n){
            if (n=='0' || n==0) return false;
            return !isNaN(parseInt(n, 10)) && isFinite(n);
        }

        /* Объект Slider
         -----------------------------------------------*/
        function Slider(slides, navs, nav_prev, nav_next, bullets){
            this.slides = slides;
            this.navs = navs;
            this.nav_prev = nav_prev;
            this.nav_next = nav_next;
            this.bullets = bullets;

            this.index = opt.index;
            this.intervalID = 0;

            this.move_slides = function($current_slide, $next_slide, direction){
                /* Set in correct position $next_slide and move both of slides: $current_slide and $next_slide */
                var sign;  // control of moving direction
                if (direction == 'next') sign = 1;
                if (direction == 'prev') sign = -1;

                $next_slide.css({left: sign*this.slides.width()});
                $next_slide.stop().animate({left: 0}, opt.move_speed);
                $current_slide.stop().animate({left: -sign*this.slides.width()}, opt.move_speed);
            };

            this.manage_navs = function(index){
                if (!opt.loop){
                    if (index == 0) {this.nav_prev.hide();this.nav_next.show();}
                    else if (index == this.slides.length - 1) {this.nav_prev.show();this.nav_next.hide();}
                    else if (index == 1 || index == this.slides.length - 2){this.nav_prev.show();this.nav_next.show();}
                }

                this.bullets.removeClass('active').eq(index).addClass('active');
            };

            this.go = function(direction, $current_slide){
                if ($current_slide == undefined) $current_slide = this.slides.eq(this.index);

                if (direction == 'next'){
                    if (this.index == this.slides.length - 1) this.index = 0; else this.index++;
                }
                if (direction == 'prev'){
                    if (this.index == 0) this.index = this.slides.length - 1; else this.index--;
                }

                var $next_slide = this.slides.eq(this.index);

                this.manage_navs(this.index);
                this.move_slides($current_slide, $next_slide, direction);
            };

            this.go_by_index = function(index){
                var $current_slide = this.slides.eq(this.index);
                if (index < this.index){
                    this.index = index + 1;
                    this.prev($current_slide);
                } else {
                    this.index = index - 1;
                    this.next($current_slide);
                }
            };

            this.next = function($current_slide){
                this.go('next', $current_slide);
            };

            this.prev = function($current_slide){
                this.go('prev', $current_slide);
            };

            this.pause = function(){
                clearInterval(this.intervalID);
            };

            this.play = function(){
                var $this = this;
                this.intervalID = setInterval(function(){
                    $this.next();
                }, opt.auto_speed);
            };

            this.init = function(){
                this.manage_navs(this.index);
                if (opt.is_move_slide && this.slides.length > 1 && isNumber(opt.auto_speed)) this.play();
            };

            this.init();
        }

        var methods = {
            init : function(options) {
                this.each(function() {
                    if (options) $.extend(true, opt, options);

                    /* --- Set containers --- */
                    var $slider = $(this);

                    var $slide_c = $slider.find('.slide_c');
                    var $slide = $slider.find('.slide');
                    var $nav = $slider.find('.nav');
                    var $nav_prev = $nav.filter('.nav_left');
                    var $nav_next = $nav.filter('.nav_right');
                    var $bullets = $slider.find('.paginator span');

                    var slider = new Slider($slide, $nav, $nav_prev, $nav_next, $bullets);


                    /* --- Bind nav blocks --- */
                    if ($slide.length > 1){
                        slider.nav_next.bind('click', function(e){
                            e.preventDefault();
                            slider.next();
                        });
                        slider.nav_prev.bind('click', function(e){
                            e.preventDefault();
                            slider.prev();
                        });
                    }


                    /* --- Auto loop --- */
                    if (opt.is_move_slide && $slide.length > 1 && isNumber(opt.auto_speed)) {
                        var stopper_clicked = false;

                        if (opt.show_stopper == true) {
                            $slider.find('.stopper_wr').click(function(e){
                                e.preventDefault();
                                if ($(this).attr('data-status') == 'pause') {
                                    stopper_clicked = true;
                                    $(this).attr({'data-status': 'play'}).find('.stopper_play').css({'display': 'table-cell'});
                                    $(this).find('.stopper_pause').hide();
                                } else {
                                    stopper_clicked = false;
                                    $(this).attr({'data-status': 'pause'}).find('.stopper_pause').css({'display': 'table-cell'});
                                    $(this).find('.stopper_play').hide();
                                }
                            });
                        }

                        /* Pause auto loop when you hover the mouse */
                        $slider.hover(function(){
                            if (!stopper_clicked) slider.pause();
                        }, function(){
                            if (!stopper_clicked) slider.play();
                        })
                    }

                    /* --- Paginator --- */
                    $bullets.click(function(){
                        var index = $(this).index();
                        if (index != slider.index) slider.go_by_index(index);
                    });

                    /* --- Resize --- */
                    $(window).resize(function(){
                        $slide.css({'left': $slide_c.width()});
                        $slide.eq(slider.index).css({'left': 0});
                    }).resize();


                    window.addEventListener('orientationchange', function() {
                        $(window).resize();
                    });
                });
            }
        };


        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' +  method + '" is not exists for sb_slider');
        }
    };
})(jQuery);
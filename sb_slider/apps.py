from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBSliderConfig(AppConfig):
    name = 'sb_slider'
    verbose_name = _('Slider')
